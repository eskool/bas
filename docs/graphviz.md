# Graphviz

Non mais allo quoi ! Tu fais quoi là! 

* Et puis voilà
* blabla

```dot
digraph {
    md [label="^.md"]
    html [label="^.html"]
    md -> html
}
```

```dot
digraph {
    md [label="?.md"]
    html [label="?.html"]
    md -> html
}
```

```graphviz dot myfile.svg
digraph {
    md [label="%.md"]
    html [label="%.html"]
    a [label="*.html"]
    b [label="*.html"]
    md -> html
    a -> b
    md -> a
}
```

!!! ex
    ```graphviz dot myfile.svg
    digraph {
        md [label="%.md"]
        html [label="%.html"]
        a [label="*.html"]
        b [label="*.html"]
        md -> html
        a -> b
        md -> a
    }
    ```

Et puis voilà c'est tout