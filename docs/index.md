---
ignore_macros: true
presentation: true
---

# Welcome to MkDocs

## TABLOR

```vartable
\begin{tikzpicture}
\tkzTabInit[espcl=2]
    {$a$/1 , $g'(a)=2a\left(a+\frac{\sqrt{2}}{2}\right)\left(2a-\sqrt{2}\right)$/1 , $g(a)=a^{4}-a^{2}$/2}
    {$-\infty$, $-\frac{{\sqrt{{2}}}}{{2}}$, $-\frac{{\sqrt{{6}}}}{{6}}$, $0$, $\frac{{\sqrt{{6}}}}{{6}}$, $\frac{{\sqrt{{2}}}}{{2}}$, $+\infty$}
\tkzTabLine{-\infty, -, z, +, \frac{2}{9}\sqrt{6}, +, z, -, -\frac{2}{9}\sqrt{6}, -, z, +, +\infty}
\tkzTabVar{+ / $+\infty$, - / $-\frac{1}{4}$, + / $0$, - / $-\frac{1}{4}$, + / $+\infty$}
\tkzTabVal[draw]{2}{3}{0.5}{$-\frac{\sqrt{6}}{6}$}{$-\frac{5}{36}$}
\tkzTabVal[draw]{2}{3}{0.5}{$\frac{\sqrt{6}}{6}$}{$-\frac{5}{36}$}
\end{tikzpicture}
```

```vartable
\begin{tikzpicture}
   \tkzTabInit[lgt=3,espcl=1.5]
     {$x$ / 1 , $f'(x)$ / 1, $f(x)=\dfrac{1}{a(a-1)}$ / 2 }
     {$-\infty$, $-1$, $1$, $+\infty$}
   \tkzTabLine
     { , + , d , h , d, +, }
   \tkzTabVar
     { -/1 , +DH/$+\infty$, -C/0, +/1}
\end{tikzpicture}
```

```vartable
\begin{tikzpicture}
   \tkzTabInit[lgt=3,espcl=1.5]
     {$x$ / 1 , $f'(x)$ / 1, $f(x)=\sqrt{\dfrac{x-1}{x+1}}$ / 2 }
     {$-\infty$, $-1$, $1$, $+\infty$}
   \tkzTabLine
     { , + , d , h , d, +, }
   \tkzTabVar
     { -/1 , +DH/$+\infty$, -C/0, +/1}
\end{tikzpicture}
```

```vartable
\begin{tikzpicture}
   \tkzTabInit[lgt=2.75]
     { $x$/1  ,  $f'(x)$/1  ,  $f(x) = tan(x)$/2  }
     { $0$,  $\dfrac{\pi}{2}$,  $\pi$}
   \tkzTabLine
     { , + , d , + , }
   \tkzTabVar
     { -/$0$ ,  +D-/$+\infty$/$-\infty$ , +/$0$}
\end{tikzpicture}
```

## VARTABLE

```vartable
\begin{tikzpicture}
\tkzTabInit[espcl=4]
    {$x$/1 , $f'(x)$/1 , $f(x)$/2}
    {$1$,$2$,$3$,$4$}
\tkzTabLine{d,+,0,+,0,+}
\tkzTabVar{D- / $-\infty$ , R/ , + / $0$ , + / $3$}
\end{tikzpicture}
```

```vartable
\begin{tikzpicture}
\tikzset{h style/.style = {fill=red!50}}
\tkzTabInit[lgt=1,espcl=2]{$x$ /1, $f$ /2}{$0$,$1$,$2$,$3$}%
\tkzTabVar{+/ $1$ / , -CH/ $-2$ / , +C/ $5$, -/ $0$ / }
\end{tikzpicture}
```

```vartable
\begin{tikzpicture}
\tkzTabInit[color,
colorT = yellow!20,
colorC = red!20,
colorL = green!20,
colorV = lightgray!20,
lgt = 1,
espcl = 2.5]%
{$t$/1,$a$/1,$b$/1,$c$/1,$d$/1}%
{$\alpha$,$\beta$,$\gamma$}%
\end{tikzpicture}
```

```vartable
\begin{tikzpicture}
	\tkzTabInit[espcl=6]
		{$x$/1 , $f'(x)$/1 , $f(x)$/2}
		{$0$ , $\sqrt{e}$ , $+\infty$}
	\tkzTabLine{d,+,0,+,}
	\tkzTabVar{D- / $-\infty$ , R/ , + / $0$ }
	\tkzTabVal[draw]{1}{3}{0.4}{$1$}{$-e$}
\end{tikzpicture}
```

```vartable
\begin{tikzpicture}
\newcommand*{ \E}{ \ensuremath{ \mathrm{e}}}.
\tkzTabInit{$x$ /1,$g'(x)$ /1,$g(x)$ /2}
{$-\infty$,$-2$,$0$,$+\infty$}
\tkzTabLine{,+,z,-,z,-}
\tkzTabVar{-/$-\infty$, +/$2$, R/, -/$-\infty$}
\tkzTabIma{2}{4}{3}{$0$}            
\end{tikzpicture}
```

```vartable
\begin{tikzpicture}
	\tkzTabInit[espcl=6]
		{$x$/1 , $f'(x)$/1 , $f(x)$/2}
		{$0$ , $\sqrt{e}$ , $+\infty$}
	\tkzTabLine{d,+,0,+,}
	\tkzTabVar{D- / $-\infty$ , R/ , + / $0$ }
	\tkzTabVal[draw]{1}{3}{0.4}{$1$}{$-e$}
\end{tikzpicture}
```

```vartable
\begin{tikzpicture}
   \tkzTabInit[lgt=3,espcl=1.5]
     {$x$ / 1 , $f'(x)$ / 1, $f(x)=\sqrt{\dfrac{x-1}{x+1}}$ / 2 }
     {$-\infty$, $-1$, $1$, $+\infty$}
   \tkzTabLine
     { , + , d , h , d, +, }
   \tkzTabVar
     { -/1 , +DH/$+\infty$, -C/0, +/1}
\end{tikzpicture}
```

```vartable
\begin{tikzpicture}
   \tkzTabInit[lgt=2.75]
     { $x$/1  ,  $f'(x)$/1  ,  $f(x) = tan(x)$/2  }
     { $0$,  $\dfrac{\pi}{2}$,  $\pi$}
   \tkzTabLine
     { , + , d , + , }
   \tkzTabVar
     { -/$0$ ,  +D-/$+\infty$/$-\infty$ , +/$0$}
\end{tikzpicture}
```

```vartable
\begin{tikzpicture}
	\tkzTabInit[espcl=6]
		{$x$/1 , $f'(x)$/1 , $f(x)$/2}
		{$0$ , $\sqrt{e}$ , $+\infty$}
	\tkzTabLine{d,+,0,+,}
	\tkzTabVar{D- / $-\infty$ , R/ , + / $0$ }
	\tkzTabVal[draw]{1}{3}{0.4}{$1$}{$-e$}
\end{tikzpicture}
```

!!! ex
    ```vartable
    \begin{tikzpicture}
        \tkzTabInit[lgt=3, espcl=6, deltacl=0.7]{$x$ /1, $(e^x)'=e^x$ /1, $e^x$ /1.5} {$-\infty$ , $+\infty$}
    \tkzTabLine{, -,}
    \tkzTabVar{-/ , +/ }
    \tkzTabVal{1}{2}{0.3}{$0$}{1}
    \tkzTabVal{1}{2}{0.6}{$1$}{$e$}
    \end{tikzpicture}
    ```

